import sqlite3
conn = sqlite3.connect('x10.db')

c = conn.cursor()

#c.execute("INSERT INTO log VALUES (?,?,?,?,?,?,?)",\
    #(datestring,timestamp,housecode,unit, hu, x10_function))
# Create table
#c.execute('''CREATE TABLE log 
#		(date1 text, date2 integer, housecode text, unitcode text, hu text, command text)''')
c.execute('''CREATE TABLE driveway 
		(date1 text, date2 integer, direction text)''')

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
