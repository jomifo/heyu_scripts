#!/home/joe/.virtualenvs/x10/bin/python
"""
Driveway alert script.   Called when any of two motion sensors is triggered.
Checks to see if other motion sensor was triggered w/in set time interval.
If so, then considers the motion trigger a "real" event and turns on chime.
Also logs the event in the driveway database table.

By using this approach, false positives are reduced and the direction of
travel (leaving or arriving) can be determined.
"""
import os
import sqlite3

from heyu import heyu

# constants
UNITS = ('C6','C7')
CHIME = 'A3'
INTERVAL = 7 # time in seconds to check for other motion sensor
ARRIVING = 'arriving'
LEAVING = 'leaving'

housecode = os.environ.get('X10_Housecode')
unit = os.environ.get('X10_Unit')
hu = '%s%s' % (housecode,unit)
timestamp = int(os.environ.get('X10_Timestamp'))
datestring = os.environ.get('X10_DateString')
x10_function = os.environ.get('X10_function').lower()
direction = None

if hu == UNITS[0]:
  hu_other = UNITS[1]
  direction = ARRIVING
elif hu == UNITS[1]:
  hu_other = UNITS[0]
  direction = LEAVING

# create db connection
conn = sqlite3.connect('/home/joe/.virtualenvs/x10/src/x10.db')
c = conn.cursor()

# look to see if other motion sensor was triggered w/in INTERVAL seconds ago
sql = "select * from log where hu = '%s' and date2 >strftime('%%s','now')-%s"
sql = sql % (hu_other, INTERVAL)
results = c.execute(sql)
rows = results.fetchall()

# do chime if any records returned
if len(rows) > 0:
    heyu('on',CHIME)
    # record driveway activity
    c.execute("INSERT INTO driveway VALUES (?,?,?)",\
        (datestring,timestamp,direction))


# record all motion sensor activity
c.execute("INSERT INTO log VALUES (?,?,?,?,?,?)",\
    (datestring,timestamp,housecode,unit, hu, x10_function))

conn.commit()
conn.close()
